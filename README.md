# <center>Documentation</center>

[[_TOC_]]

## Problématique:
Le contexte était lors du projet de décommissionnement de l'OSB (Oracle Service Bus) dans une entreprise (puisque l'OSB coûte très cher et il ne sera plus supporté à partir de 2021). Donc cette entreprise a pris la décision de migrer de OSB à APIM (OpenAPI).
Dans certains cas, la migration est coûteuse en terme de temps et effort car il s'agit des anciennes applications  avec de configuration fortement couplée.
Du coup et dans cadre d'un projet de fin d'année en binôme, nous avons développé une solution pour accompagner la  migration des Web Services.

## Solution:
En fait, nous avons travaillé sur 2 piliers:
Le premier pilier était la mise en place d'un proxy (nginx) permettant de:
- forcer la communication HTTPS avec l'implémentation des certificats SSL.
- rediriger les requêtes selon le besoin.
- traduire les Web Services de l'XML à Json.
- gèrer les certificats client.

(Voir figure ci-dessous)
![nginx.png](/img/nginx.png)

Le 2éme pilier, principalement ma tache, était l'automatisation de la mise en place (avec **Terraform**) et la configuration (avec **Ansible**) de l'environnement technique sur **Azure**.

(Voir figure ci-dessous)
![plan.png](/img/plan.png)

