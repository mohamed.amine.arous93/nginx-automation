#!/usr/bin/python

import json, os

def get_public_ip():
    with open("../terraform/.terraform/output.json",) as tfstate:
        data = json.load(tfstate)
        return data["public_ip_address"]["value"]

def get_inventory():
    ip = get_public_ip()
    return {"nodes": {
                "hosts": [ip],
                "vars": {
                    "ansible_python_interpreter": "/usr/bin/python3",
                    "host_key_checking": "False",
                    "ansible_ssh_user": os.environ["TF_VAR_admin_username"],
                    "ansible_ssh_pass": os.environ["TF_VAR_admin_password"],
                    }
                }
            }

print(json.dumps(get_inventory()))
